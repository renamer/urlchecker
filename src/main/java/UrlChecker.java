import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UrlChecker {

    private static int threadNumber = 10;

    public static void main(String[] args) throws Exception {

        if (args.length < 1) {
            System.out.println("Please provide the path to .csv file as the first parameter. The second optional " +
                    "parameter is the number or threads, default value is 10");
            System.exit(1);
        }

        String fileName = args[0];

        if (args.length == 2){
            try {
                threadNumber = new Integer(args[1]);
            } catch (NumberFormatException nfe){
                System.out.println("can not parse the parameter " + args[1] + ": it should be integer number - number of threads");
                System.exit(1);
            }

        }

        ExecutorService executor = Executors.newFixedThreadPool(threadNumber);

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            boolean firstLine = true;
            while ((line = br.readLine()) != null) {
                if (firstLine){
                    System.out.println(line);
                    firstLine = false;
                    continue;
                }
                Runnable checker = new CheckerThread(line);
                executor.execute(checker);
            }
        } finally {
            executor.shutdown();
        }


    }




}
