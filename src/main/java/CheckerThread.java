
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.net.URLEncoder;
import java.net.UnknownHostException;

public class CheckerThread implements Runnable {

    private String line;

    public CheckerThread(String line) {
        this.line = line;
    }

    @Override
    public void run() {
        processCommand();
    }

    public void processCommand() {
        String urlStr = extractUrl();
        int code = checkTheUrl(urlStr);
        System.out.println("code: " + code);
        if (code == 200 ) {
            System.out.println(line);

        }
        //System.out.println(code + "," + line);
    }

    public String extractUrl(){
        if (line.startsWith("\"")){
            String s1 = line.substring(1);
            return line.substring(1, s1.indexOf("\"") + 1);
        }
        return line.substring(0, line.indexOf(","));
    }

    public int checkTheUrl(String str) {
        try {
            System.out.println("process str " + str);
            CloseableHttpClient  client = HttpClientBuilder.create().
                    setRetryHandler(new DefaultHttpRequestRetryHandler(0, false)).build();
            HttpParams params = new BasicHttpParams();
            //params.setIntParameter("http.connection.timeout", 50);
            params.setParameter("http.protocol.handle-redirects",false);
            str = URLEncoder.encode(str, "UTF-8");
            str = str.replace("%3A%2F%2F", "://");
            str = str.replaceAll("%2F", "/");
            HttpGet request = new HttpGet(str.replaceAll(" ", "%20"));
            request.setParams(params);
            request.setHeader("Referer", "http://example.com");
            HttpResponse httpResp = client.execute(request);
            return httpResp.getStatusLine().getStatusCode();
        } catch (IOException ioe) {
            if (ioe instanceof UnknownHostException) {
                return 404;
            } else {
                return 500;
            }

        }

    }


}
