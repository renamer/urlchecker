# URL checker

The small utility to check URLs availability

The utility need a CSV file as input and consider the first column as the URL, checks if the URL available 
and print the original line if so.

The program is multithread, so it is possible to provide the number of thread to run in parallel as a second parameter


To run probram:

java -jar urlchecker-1.0.0-jar-with-dependencies.jar PATH_TO_CSV_THERE.CSV 40 > result.csv

